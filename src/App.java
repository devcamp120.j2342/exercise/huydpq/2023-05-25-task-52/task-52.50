import java.util.ArrayList;
import java.util.Date;

import com.devcamp.j03_javabasic.s50.Order;

public class App {
    public static void main(String[] args) throws Exception {
        ArrayList<Order> arr = new ArrayList<>();
        // Khởi tạo Order với các tham số khác nhau ( tap 4 Ojb Order)
        Order order1 = new Order();
        Order order2 = new Order("Decam");
        Order order3 = new Order(12, "huy", 120000);
        Order order4 = new Order(4, "Hùng",50000, new Date(), false, new String[] {"hộp màu", "tay", "giay mau"});
        arr.add(order1);
        arr.add(order2);
        arr.add(order3);
        arr.add(order4);
        for(Order order : arr){
            System.out.println(arr.toString());
        }
    }
}
